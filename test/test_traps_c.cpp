#include "tipo.h"
#include "types.h"

#define N_REG  33

//__attribute__ ((aligned (16))) char stack0[4096];

// descrittore di processo
struct des_proc {
	uint16 id;
	uint16 livello;
	uint32 precedenza;
	uint64 punt_nucleo;
	uint64 contesto[N_REG];
	uint64 epc;
	uint64 satp;
	// paddr cr3; TODO: Insert pagination info

	des_proc *puntatore;
};
static des_proc *esecuzione;
static des_proc init;
static des_proc *esecuzione_precedente;

extern "C" void strap();
extern "C" void ktrap();
extern "C" void writeSTVEC(void*);
extern "C" uint64 readSEPC();
extern "C" int readSCAUSE();
extern "C" void disableSInterrupts();
extern "C" void sInterruptReturn();
extern "C" void clearSPreviousPrivilege();
extern "C" void setSPreviousInterruptEnable();

extern "C" void sInterruptHandler(){

    int cause = 0;

    //Imposta il trap vector all'handler 
    //per il kernel
    writeSTVEC((void*)ktrap);

    //Salva l'indirizzo a cui si deve tornare
    //al termine della gestione dell'int
    esecuzione->epc = readSEPC();

    //Determina il codice dell'interruzione
    cause = readSCAUSE();

    switch(cause){

    }

    sInterruptReturn();
}

extern "C" void sInterruptReturn(){

    //Potremmo venire da codice che aveva precedentemente eseguito
    //in modalita' supervisor con le interruzioni abilitate.
    //Prima di fare qualsiasi cosa disattiviamole.
    disableSInterrupts();

    //Ripristiniamo l'interrupt handler da usare al di fuori del kernel
    writeSTVEC((void*)strap);

    //Indichiamo il livello utente come privilegio a cui tornare
    clearSPreviousPrivilege();

    //Abilitiamo interruzioni per l'utente al ritorno
    setSPreviousInterruptEnable();



}

extern "C" void kInterruptHandler(){

}