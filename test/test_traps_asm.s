.global kInterruptHandler
.global ktrap
ktrap:
    addi sp,sp,-8
    sd ra,0(sp)
    call salva_stato
    addi sp,sp,8

    call kInterruptHandler

    call carica_stato

.global sInterruptHandler
.global strap
strap:
    addi sp,sp,-8
    sd ra,0(sp)
    call salva_stato
    addi sp,sp,8

    call sInterruptHandler

.global writeSTVEC
writeSTVEC:
    csrw stvec, a0
    ret

.global readSEPC
readSEPC:
    csrr a0, sepc
    ret

.global readSCAUSE
readSCAUSE:
    csrr a0, scause
    ret

# Supervisor interrupts are enabled/disabled
# by writing to the 2nd bit of sstatus
.global enableSInterrupts
enableSInterrupts:
    addi sp,sp,-8
    sd a0,0(sp)
    csrr a0, sstatus
    or a0,a0,2         # ...0010
    csrw sstatus, a0
    ld a0,0(sp)
    ret

# Supervisor interrupts are enabled/disabled
# by writing to the 2nd bit of sstatus
.global disableSInterrupts
disableSInterrupts:
    addi sp,sp,-16
    sd a0,0(sp)
    sd a1,8(sp)
    csrr a0, sstatus
    li a1,2         # ...00010
    not a1, a1      # ...11101
    and a0,a0,a1         
    csrw sstatus, a0
    ld a0,0(sp)
    ret
    

# Clear S Previous Privilege mode (User mode)
.global clearSPreviousPrivilege
clearSPreviousPrivilege:
    addi sp,sp,-16
    sd a0,0(sp)
    sd a1,8(sp)
    csrr a0, sstatus
    li a1,256           # ...0100000000
    not a1, a1          # ...1011111111
    and a0,a0,a1         
    csrw sstatus, a0
    ld a0,0(sp)
    ret

# Set S Previous Interrupt Enable
.global setSPreviousInterruptEnable
setSPreviousInterruptEnable:
    addi sp,sp,-8
    sd a0,0(sp)
    csrr a0, sstatus
    or a0,a0,32         # ...00100000
    csrw sstatus, a0
    ld a0,0(sp)
    ret