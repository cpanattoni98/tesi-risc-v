# readCR2 legge il contenuto di CR2, che in un'architettura
# x86 contiene l'indirizzo virtuale che era in fase di traduzione.
# Sfruttiamo questo registro durante un PageFault.
# In RISCV, l'indirizzo colpevole viene salvato nel registro stval.
.global readSTVAL
readSTVAL:
	.cfi_startproc
	csrr a0, stval
	ret
	.cfi_endproc

